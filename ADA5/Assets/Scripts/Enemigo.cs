using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{

    public GameManager gameManager;
    public Jugador jugador;
    public Rigidbody rb;

    public AudioSource golpe;

    Vector3 movimiento;
    public float velocidad;

    public float altura;
    public bool spawneando;
    public bool funcional;

    public float danyo;
    public bool ataque;
    public float cooldownAtaque;
    public float cooldownActual;

    public float alturaDestruccion;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        spawneando = true;
        funcional = false;
        ataque = false;
    }

    // Update is called once per frame
    void Update()
    {
        Spawneando();
        Movimiento();
        Destruccion();
    }



    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player") && ataque == true)
        {
            jugador.vida = jugador.vida - danyo;
            ataque = false;
            cooldownActual = cooldownAtaque;
            Debug.Log("Tocado");
        }

        if(ataque == false)
        {
            if (cooldownActual > 0)
            {
                cooldownActual = cooldownActual - (1 * Time.deltaTime);
            }
            else if (cooldownActual <= 0)
            {
                ataque = true;
            }
        }
    }


    void Spawneando()
    {
        if (transform.position.y > altura)
        {
            Debug.Log("Aparezco");
        }
        else if (transform.position.y <= altura && spawneando == true)
        {
            transform.position = new Vector3(transform.position.x, altura, transform.position.z);
            spawneando = false;
            funcional = true;
            ataque = true;
        }
    }

    void Movimiento()
    {
        if(funcional == true)
        {
            transform.LookAt(jugador.transform.position);
            movimiento = transform.forward * velocidad * Time.deltaTime;
            transform.position = transform.position + new Vector3(movimiento.x, 0, movimiento.z);
        }
    }


    //Este creo que no nos hace falta igual lo dejo aqui por si a caso
    public void Muerte()
    {
        golpe.Play();
        if(funcional == true)
        {
            funcional = false;
            gameManager.conteoEnemigos = gameManager.conteoEnemigos + 1;
        }
    }

    void Destruccion()
    {
        if (transform.position.y <= alturaDestruccion)
        {
            Destroy(gameObject);
        }
    }
}
