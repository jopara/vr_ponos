using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Punyos : MonoBehaviour
{

    // Cuando pega activa el enemigo.Muerte(), intentar que sea a partir de cierta velocidad y no siempre para que no pegue sin mover fdo alex
    // enemigo.Muerte() a�ade por si mismo al contador, no hace falta que se a�ada desde aqui
    // enemigo.Muerte tiene que formar parte de la funcion del pu�etazo, ademas de a�adir la fuerza

    public float fuerzaHostia;
    public GameManager gamemanager;
    public Enemigo enemigo;
    public Jugador jugador;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemigo")
        {
            collision.gameObject.GetComponent<Rigidbody>().AddForce(enemigo.transform.position - jugador.transform.position * fuerzaHostia);
            collision.gameObject.GetComponent<Enemigo>().Muerte();
        }
    }

    
}
