using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaUI : MonoBehaviour
{
    public Material material;
    public Jugador jugador;

    // Start is called before the first frame update
    void Start()
    {
        //material.SetFloat("Vector1_cdbc4c80a70445968ed3da73f40331be", jugador.vida);
        material.SetFloat("Vector1_68789801eb7e47fba5a76f348ec34e51", jugador.vida);
    }

    // Update is called once per frame
    void Update()
    {
        //material.SetFloat("Vector1_cdbc4c80a70445968ed3da73f40331be", jugador.vida);
        material.SetFloat("Vector1_68789801eb7e47fba5a76f348ec34e51", jugador.vida);
    }
}
