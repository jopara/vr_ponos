using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public int conteoEnemigos; // Esto cuenta la cantidad de enemigos matados
    
    public bool muerteJugador;



    public GameObject CanvasPausa;
    public GameObject CanvasGui;
    public GameObject CanvasHasGanado;
    public GameObject CanvasHasPerdido;

    public Canvas canvasPausa;
    public Canvas canvasGui;
    public Canvas canvasHasGanado;
    public Canvas canvasHasPerdido;

    public Text contadorText;



    // Cosas que sobran pero dejamos por aqui por si a caso

    int muertesEnemigos; // Esto sobra
    public GameObject Enemigos; // El enemigo a�ade desde su funcion asi que no hace falta;



    // Start is called before the first frame update
    void Start()
    {
        canvasGui.enabled = true;
        canvasPausa.enabled = true;
        canvasHasGanado.enabled = true;
        canvasHasPerdido.enabled = true;

        CanvasGui.SetActive(true);
        CanvasPausa.SetActive(false);
        CanvasHasGanado.SetActive(false);
        CanvasHasPerdido.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        VerKills();
        //AnyadirEnemigo();
    }
    
    // A lo mejor podemos pasarlo al update
    private void VerKills()
    {
        contadorText.text = conteoEnemigos.ToString();
    }

    void MuerteJugador()
    {
        CanvasHasPerdido.SetActive(true);

    }

    public void Playlvl()
    {
        SceneManager.LoadScene("JuegoPrincipal");
    }

    public void QuitApp()
    {
        Application.Quit();
    }


    /*
    public void AnyadirKill()
    {
        muertesEnemigos++;
    }
    */
    /*
    public void AnyadirEnemigo()
    {
        
    }
    */


}
