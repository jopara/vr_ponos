using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class Jugador : MonoBehaviour
{
    public XRNode inputSource;
    private Vector2 inputAxis;
    public CharacterController jugador;
    private XRRig rig;

    public float velocidadJugador;
    public float additionalHeight;

    public float vida;
    public float vidaMaxima;
    public float regeneracion;
    public bool perdido;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GetComponent<CharacterController>();
        rig = GetComponent<XRRig>();
    }

    // Update is called once per frame
    void Update()
    {
        InputDevice device = InputDevices.GetDeviceAtXRNode(inputSource);
        device.TryGetFeatureValue(CommonUsages.primary2DAxis, out inputAxis);
        
        RegeneracionVida();
    }

    private void FixedUpdate()
    {
        CapsuleFollowHeadSet();
        Quaternion headYaw = Quaternion.Euler(0, rig.cameraGameObject.transform.eulerAngles.y, 0);
        Vector3 direction = headYaw * new Vector3(inputAxis.x, 0, inputAxis.y);
        jugador.Move(direction * Time.fixedDeltaTime * velocidadJugador);
    }

    void CapsuleFollowHeadSet()
    {
        jugador.height = rig.cameraInRigSpaceHeight + additionalHeight;
        Vector3 capsuleCenter = transform.InverseTransformPoint(rig.cameraGameObject.transform.position);
        jugador.center = new Vector3(capsuleCenter.x, jugador.height/2 + jugador.skinWidth, capsuleCenter.z);
    }

    void RegeneracionVida()
    {
        if (vida < vidaMaxima && perdido == false)
        {
            vida = vida + (regeneracion * Time.deltaTime);
            vida = Mathf.Clamp(vida, 0, vidaMaxima);
        }
    }
}
