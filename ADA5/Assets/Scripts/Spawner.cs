using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject enemigo;

    public float radio;
    public float radioMin;
    public float radioMax;

    public float altura;

    public float angulo;

    public float spawnear;
    public float spawnMin;
    public float spawnMax;

    Transform puntoSpawn;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Manejo del radio de spawn
        radio = Random.Range(radioMin, radioMax);
        puntoSpawn = this.gameObject.transform.GetChild(0);
        puntoSpawn.transform.position = new Vector3(radio, altura, 0);

        //Manejo del angulo de spawn
        angulo = Random.Range(0, 360);
        transform.rotation = Quaternion.Euler(0, angulo, 0);    

        if (spawnear <= 0f)
        {
            Spawn();
            spawnear = Random.Range(spawnMin, spawnMax);
        }

        else if (spawnear > 0f)
        {
            spawnear = spawnear - Time.deltaTime;
        }
    }

    void Spawn()
    {
        Debug.Log("Spawn");
        Instantiate(enemigo, puntoSpawn.transform.position, Quaternion.identity);
    }
}
